(function() {
  'use strict'

  angular.module('app', ['ui.router', 'app.constant']).run(runner)

  runner.$inject = ['$rootScope', 'CONSTANT']

  function runner(root, CONSTANT) {
    root.title = CONSTANT.TITLE
  }
})()