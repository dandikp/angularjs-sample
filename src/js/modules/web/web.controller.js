(function(){
  'use strict'
  angular.module('app').controller('WebController', WebController)

  WebController.$inject = ['$rootScope', '$scope']

  function WebController ($rootScope, $scope) {
    var vm = this
    vm.title = 'MAIN WEB'
    $scope.subtitle = 'Coba AngularJS'
  }
})()