(function () {
  'use strict'

  angular.module('app').config(config)

  config.$inject = ['$stateProvider', '$urlRouterProvider']

  function config($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('web', {
        url: '/',
        controller: 'WebController',
        templateUrl: 'src/js/modules/web/web.html',
        controllerAs: 'vm'
      })
      .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'src/js/modules/dashboard/dashboard.html'
      })
      .state('dashboard.employee', {
        url: '/employee',
        templateUrl: 'src/js/modules/dashboard/employee/employee.html'
      })
      .state('404', {
        url: '/404',
        templateUrl: 'src/js/modules/error-page/404.html'
      })
    $urlRouterProvider.when('', '/')
    $urlRouterProvider.otherwise('/404')
  }
})()