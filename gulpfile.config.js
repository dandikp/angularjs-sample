module.exports = {
  dev: {
    css: [
      './src/assets/css/**/*.css'
    ],
    js: [
      './node_modules/angular/angular.js',
      './node_modules/@uirouter/angularjs/release/angular-ui-router.js',
      './app.*.js',
      './src/constant/*.js',
      './src/js/modules/**/*.js',
      './src/js/partials/**/*.js'
    ]
  }
}